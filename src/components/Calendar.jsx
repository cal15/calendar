 import React , { useEffect, useState } from 'react'
import { Calendar, dateFnsLocalizer } from 'react-big-calendar'
import format from 'date-fns/format'
import parse from 'date-fns/parse'
import startOfWeek from 'date-fns/startOfWeek'
import getDay from 'date-fns/getDay'
import enUS from 'date-fns/locale/en-US'
import 'react-big-calendar/lib/css/react-big-calendar.css';
import Popping from './Popping';
import {closeEvent, ShowEventApi, ShowEventsApi} from "../Redux/actions"
import { connect } from 'react-redux'
import AddEvents from './AddEvents'
import { Modal, Button } from "react-bootstrap";
import {event} from "../Redux/Axios/event"
import { addError, removeError } from "../Redux/actions/errorsAction"
import * as moment from "moment"


const id =34

const locales = {
  'en-US': enUS,
}

const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
})


const MyCalendar = ({events, ShowEventApi, closeEvent, ShowEventsApi}) => {
  const [open, setOpen] = useState(false);
  const [renderStatus, rerender] = useState(false);

  const [eventForm,showEventForm]=useState(false)

  useEffect(()=>{
    ShowEventsApi()
    console.log("i renderd because of refresh or start");
  },[])

  const handleClose=()=>{
    showEventForm(false)
  }
  useEffect(()=>{
    ShowEventsApi()
    console.log("i renderd");
  },[renderStatus])
 

  const openEventClick = (event)=>{
    alert('hiiiii')
       setOpen(true)
       if(event.id) {
        ShowEventApi( event.id);
       }
       
       return;
  }

  const closeEventClick = () =>{
    setOpen(false);
    setTimeout(()=>closeEvent(),300) ;
  }
  const addEvent = (newEvent)=>{
    return{
      type: "ADD_EVENT",
      payload: newEvent
    }
}
   const addEventApi = (values) => async dispatch =>{
    const result = await event.post("/", {
         title: values.title,
         start: values.start,
         end: values.end,
         describe: values.describe
       })
       .then(res=>{
        
        if(res && res.data){
            console.log("event from the api going to the reducer: ", res.data)
            dispatch(addEvent(res.data)) 
            dispatch(removeError())
            
            return  "success";
        }
       })
       .catch(res=>{
        console.log("catch response, ", res)
        if(res.response.data){
            
            console.log(res.response.data)
            dispatch(addError(res.response.data));
        }
    })
       
}
   const getEventApi = async()  => {
     
    //i won't get the event from redux store as it is safer to
    //keep updated with db.
    const result = await event.get(`/${id}/show`);

    try{
        const {title, _id, start, end, describe} =  result.data;
        const convertedEvent = {
            title,
            describe,
            id: _id,
            start: moment(start).format("ddd DD MMM YY LT"),
            end: moment(end).format("ddd DD MMM YY LT")
        }
        // await dispatch(showEvent(convertedEvent))
    }catch(err){
         const error =await err.data.message;
         return error
    }
}
  
  return (
  <div>
      <Popping open={open}
       handleOpen={openEventClick} 
       handleClose={closeEventClick} 
       renderStatus = {renderStatus} 
       rerender= {rerender}/>
      <Calendar        
      selectable
          localizer={localizer}
          events={events}
          startAccessor="start"
          endAccessor="end"
          style={{ height: 500 , margin: 50, fontFamily: 'Patrick Hand' }}
          onSelectEvent={openEventClick}
          onDoubleClickEvent={()=>alert('112223455')}
          openEventClick={()=>alert('aaaa')}
          onSelecting={()=>alert('bbbb')}
// onDrillDown={()=>alert('hiiiii')}
            // onRangeChange={()=>alert('hiiiii')}
            onSelectSlot={()=>showEventForm(true)}
            // onView={()=>alert('hiiiii')}
        />


<Modal show={eventForm} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title className="text-capitalize">{'Fazal'}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <AddEvents />

          </Modal.Body>
        
      </Modal>

       
    </div>
        
    )
}

function mapStateToProps({event, events}){
  return{
    event,
    events
  }
}

export default connect(mapStateToProps, {ShowEventApi, closeEvent, ShowEventsApi})(MyCalendar)

